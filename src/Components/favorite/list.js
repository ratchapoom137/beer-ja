import React, { Component } from 'react';
import { List } from "antd"
import ItemFavorite from '../beer/itemBeer'

class ListFavorite extends Component {
    // state = {
    //     items: []
    // }

    // componentDidMount() {
    //     const jsonUserStr = localStorage.getItem('user-data');
    //     const email = jsonUserStr && JSON.parse(jsonUserStr).email;
    //     const jsonFavStr = localStorage.getItem(`beer-ja-list-fav-${email}`)
    //     if (jsonFavStr) {
    //         const items = JSON.parse(jsonFavStr)
    //         this.setState({ items })
    //     }
    // }

    render() {
        return (
            <div style={{ minHeight: '300px'}}>
                <List
                    pagination={{
                        pageSize: 12,
                    }}
                    grid={{ gutter: 16, column: 4 }}
                    dataSource={this.props.items}
                    renderItem={item => (
                        <List.Item>
                            <ItemFavorite
                                item={item}
                                // onItemBeerClick={this.props.onItemBeerClick}
                            />
                        </List.Item>
                    )}
                />
            </div>
        );
    }
}

export default ListFavorite
