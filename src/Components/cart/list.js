import React, { Component } from 'react';
import { List, Icon } from "antd"
import ItemChart from '../beer/itemBeer'

class ListCart extends Component {
    // state = {
    //     items: []
    // }

    // componentDidMount() {
    //     const jsonUserStr = localStorage.getItem('user-data');
    //     const email = jsonUserStr && JSON.parse(jsonUserStr).email;
    //     const jsonCartStr = localStorage.getItem(`beer-ja-list-cart-${email}`)
    //     if (jsonCartStr) {
    //         const items = JSON.parse(jsonCartStr)
    //         this.setState({ items })
    //     }
    // }

    total() {
        return this.props.items.reduce((sum, item) => {
            return sum+item.attenuation_level
        }, 0);
    }

    render() {
        return (
            <div style={{ minHeight: '300px' }}>
            <h2 style={{ marginLeft: '850px', color: '#000099',}}>
                Total price: {this.total()}&nbsp;<Icon type="dollar" theme="twoTone" />
            </h2>
                <List
                    pagination={{
                        pageSize: 12,
                    }}
                    grid={{ gutter: 16, column: 4 }}
                    dataSource={this.props.items}
                    renderItem={item => (
                        <List.Item>
                            <ItemChart
                                item={item}
                                // onItemBeerClick={this.props.onItemBeerClick}
                            />
                        </List.Item>
                    )}
                />
            </div>
        );
    }
}

export default ListCart
