import React from 'react'
import ItemBeer from './itemBeer'
import { List } from "antd"

function ListBeer(props) {
    return (
        <div style={{ minHeight: '300px' }}>
            <List
                // pagination={{
                //     pageSize: 12,
                // }}
                grid={{ gutter: 16, column: 4 }}
                dataSource={props.items}
                renderItem={item => (
                    <List.Item>
                        <ItemBeer item={item} onItemBeerClick={props.onItemBeerClick} />
                    </List.Item>
                )}
            />
        </div>
    );
}

export default ListBeer;