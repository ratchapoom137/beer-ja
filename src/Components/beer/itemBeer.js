import React from 'react'
import { Card } from 'antd';
import TextTruncate from 'react-text-truncate';
import { connect } from 'react-redux';

const { Meta } = Card;

const mapDispatchToProps = dispatch => {
    return {
        onItemBeerClick: item =>
            dispatch({
                type: 'click_item',
                payload: item
            })
    };
};

// redux เข้ามาช่วยเรื่องของ cycle prop
function ItemBeer(props) { // prop เรามาสามารถกำหนด prop ได้ว่าจะเอา object แบบไหนเข้ามาได้บ้างโดยใช้ prop-types
    const item = props.item
    return (
        <Card
            title={item.name}
            onClick={() => {
                props.onItemBeerClick(item);
            }}
            hoverable
            cover={
                <div>
                    <img
                        src={item.image_url}
                        style={{ height: '200px', width: 'auto', paddingTop: '16px' }}
                    />
                </div>
            }
        >
            <Meta
                title={"Price : "+item.attenuation_level+"$"}
                description={
                    <TextTruncate
                        line={1}
                        truncateText="..."
                        text={item.description}
                        textTruncateChild={<a href="#">Read more</a>}
                    />
                }
            />
        </Card>
    );
}

export default connect(null, mapDispatchToProps)(ItemBeer);