import React, { Component } from 'react';
import { Spin, Modal, Button, Layout, Menu, message, Icon, Badge, Dropdown, Avatar } from 'antd';
// import ListBeer from '../Components/beer/listBeer';
import RouteMenu from './RouteMenu';
import { connect } from 'react-redux';

const { Header, Content, Footer } = Layout;
const { SubMenu } = Menu;

const menus = ['home', 'favorite', 'cart', 'profile'];

const mapStateToProps = state => {
    return {
        isShowDialog: state.isShowDialog,
        itemBeer: state.itemBeer
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onDismissDialog: () =>
            dispatch({
                type: 'dismiss_dialog'
            }),
        onItemBeerClick: item =>
            dispatch({
                type: 'click_item',
                payload: item
            })
    };
};
  
  

class MainPage extends Component {
    state = {
        items: [],
        isShowModal: false,
        itemBeer: null,
        pathName: menus[0],
        favItems: [],
        cartItems: [],
        email: '',
        isLoading: false,
        imageUrl: ''
    };

    onClickLogout = () => {
        localStorage.setItem(
        'user-data',
        JSON.stringify({
            isLoggedIn: false
        })
        );
        setTimeout(() => {
            this.props.history.push('/');
        }, 2000);
    };

    menu = (
        <Menu onClick={this.onClickLogout}>
          <Menu.Item >Log Out</Menu.Item>
        </Menu>
      );

    // onItemMovieClick = (item) => {
    //     //TODO: show detail movie
    //     this.setState({ isShowModal: true, itemMovie: item })
    // }

    onModalClickOK = () => {
        //TODO: handdle somethis on click ok
        //this.setState({ isShowModal: false })
        this.props.onDismissDialog();
    }

    onModalClickCancel = () => {
        //TODO: handdle somethis on click cancel
        // this.setState({ isShowModal: false })
        this.props.onDismissDialog();
    }

    componentDidMount() {
        const jsonUserStr = localStorage.getItem('user-data');
        const email = jsonUserStr && JSON.parse(jsonUserStr).email;

        const jsonFavStr = localStorage.getItem(`beer-ja-list-fav-${email}`)
        const jsonChartStr = localStorage.getItem(`beer-ja-list-cart-${email}`)

        if (jsonFavStr) {
            const items = jsonFavStr && JSON.parse(jsonFavStr)
            this.setState({ favItems: items })
        }
        if (jsonChartStr) {
            const items = jsonChartStr && JSON.parse(jsonChartStr)
            this.setState({ cartItems: items })
        }

        var imageUrl = jsonUserStr && JSON.parse(jsonUserStr).imageUrl;
        if (!imageUrl) {
            imageUrl = 'https://icons-for-free.com/free-icons/png/512/1902268.png';
          }
          this.setState({email, imageUrl });

        const { pathname } = this.props.location;
        var pathName = menus[0];
        if (pathname != '/') {
            pathName = pathname.replace('/', '');
            if (!menus.includes(pathName)) pathName = menus[0];
        }
        this.setState({ pathName, email });
        // TODO: get list movie from API
        fetch('https://api.punkapi.com/v2/beers?page=1&per_page=40')
            .then(response => response.json())
            .then(items => this.setState({ items }))
    }

    onMenuClick = e => {
        //TODO: switch component
        // var path = '/';
        // if (e.key != '/home') {
        //     path = `/${e.key}`;
        // }
        // this.props.history.replace(path);
        
        if (e.key) {
            var path = '/';
            path = `/${e.key}`;
            this.props.history.replace(path);
          }
    }

    onClickFavorite = () => {
        //TODO: save item to localstorage
        const items = this.state.favItems
        const itemFav = this.props.itemBeer
        const index = items.findIndex(item => {
            return item.name === itemFav.name;
        });

        if (index != -1) {
            items.splice(index, 1);
            localStorage.setItem(
                `beer-ja-list-fav-${this.state.email}`,
                JSON.stringify(items)
            );
            message.success('Unfavorite this item successfully', 1, () => {
                this.setState({ favItems: items });
                this.onModalClickCancel();
            });
        } else {
            items.push(itemFav);
            localStorage.setItem(
                `beer-ja-list-fav-${this.state.email}`,
                JSON.stringify(items)
            );
            message.success('Saved your favorite this item', 1, () => {
                this.setState({ favItems: items });
                this.onModalClickCancel();
            });
        }


        // const result = items.find(item => {
        //     return item.name === itemClick.name
        // })

        // if (result) {
        //     //TODO: show error when click favorite 
        //     message.error('This item added favorite', 1)
        // } else {
        //     items.push(itemClick)
        //     localStorage.setItem('list-fav', JSON.stringify(items))
        //     message.success('Saved your favorite beers', 1);
        //     this.onModalClickCancel();
        // }
    };

    onClickBuyTicket = () => {
        //TODO: save item to localstorage
        const items = this.state.cartItems
        const itemCart = this.props.itemBeer
        const index = items.findIndex(item => {
            return item.name === itemCart.name;
        });

        if (index != -1) {
            items.splice(index, 1);
            localStorage.setItem(
                `beer-ja-list-cart-${this.state.email}`,
                JSON.stringify(items)
            );
            message.success('Uncart this item successfully', 1, () => {
                this.setState({ cartItems: items });
                this.onModalClickCancel();
            });
        } else {
            items.push(itemCart);
            localStorage.setItem(
                `beer-ja-list-cart-${this.state.email}`,
                JSON.stringify(items)
            );
            message.success('Saved your cart this item', 1, () => {
                this.setState({ cartItems: items });
                this.onModalClickCancel();
            });
        }
    }

    checkItemFavorited = () => {
        const items = this.state.favItems
        const itemBeer = this.props.itemBeer
        const result = items.find(item => {
            return item.name === itemBeer.name;
        });
        if (result) {
            return 'primary';
        } else {
            return '';
        }
    };

    checkItemCarted = () => {
        const items = this.state.cartItems
        const itemBeer = this.props.itemBeer
        const result = items.find(item => {
            return item.name === itemBeer.name;
        });
        if (result) {
            return 'primary';
        } else {
            return '';
        }
    };

    navigateToCart = () => {
        this.props.history.replace('/cart');
    };

    render() {
        const item = this.props.itemBeer;
        return (
            <div>
                {
                this.state.items.length > 0 ? (
                <div style={{ height: '100vh' }}>
                    {' '}
                    <Layout className="layout" style={{ background: '#ffff99' }}>
                        <Header
                            style={{
                                padding: '0px',
                                position: 'fixed',
                                zIndex: 1,
                                width: '100%',
                                backgroundColor: 'white',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <Menu
                                theme="dark"
                                mode="horizontal"
                                defaultSelectedKeys={[this.state.pathName]}
                                style={{ lineHeight: '64px', position: 'relative' }}
                                onClick={e => {
                                    this.onMenuClick(e);
                                }}
                            >
                                <div
                                style={{
                                    position: 'absolute',
                                    top: '36%',
                                    left: '15px',
                                    transform: 'translateY(-40%)'
                                }}
                                >
                                <img src="http://icons.iconarchive.com/icons/flat-icons.com/flat/512/Beer-icon.png" height="50" width="50" />
                                &nbsp;&nbsp;Project Beer Ja
                                </div>
                                <Menu.Item key={menus[0]}><Icon type="home" theme="filled" />Home</Menu.Item>
                                <Menu.Item key={menus[1]}><Icon type="heart" theme="filled" />Favorite</Menu.Item>
                                <Menu.Item key={menus[2]}>
                                    <Badge
                                        count={this.state.cartItems.length}
                                        overflowCount={10}
                                        onClick={this.navigateToCart}
                                    >
                                        <Icon type="shopping-cart" style={{ fontSize: '20px' }} />
                                    </Badge>
                                    Cart
                                </Menu.Item>
                                <Menu.Item key={menus[3]}><Icon type="user" />Profile</Menu.Item>
                                <div
                                style={{
                                    position: 'absolute',
                                    top: '40%',
                                    right: '15px',
                                    transform: 'translateY(-40%)'
                                }}
                                >
                                {/* <Dropdown overlay={menu}>
                                <Button>
                                    <Avatar size="small" src={this.state.imageUrl} />&nbsp;&nbsp;{this.state.email}
                                </Button>
                                </Dropdown> */}
                                <Dropdown overlay={this.menu} trigger={['click']}>
                                    <a style={{color: 'white'}} className="ant-dropdown-link" href="#">
                                        <Avatar size={32} src={this.state.imageUrl} />&nbsp;&nbsp;{this.state.email}&nbsp;<Icon type="down" />
                                    </a>
                                </Dropdown>
                                </div>
                            </Menu>
                        </Header>
                        <Content >
                            <RouteMenu items={this.state.items}/>
                        </Content>
                        <Footer style={{ textAlign: 'center', background: 'white' }}>
                            BeerJa Website Application Workshop @ CAMT
                                </Footer>
                    </Layout>
                </div>
                 ) : (
                    <Spin size="large" tip="Loading..."/>
                )} 

                {
                    item != null ? (
                        <Modal
                            width="40%"
                            style={{ maxHeight: '80%' }}
                            title={item.name}
                            visible={this.props.isShowDialog}
                            //onOk={this.onModalClickOK}
                            onCancel={this.onModalClickCancel}
                            footer={[
                                <Button
                                    key="submit"
                                    type={this.checkItemFavorited()}
                                    icon="heart"
                                    size="large"
                                    shape="circle"
                                    onClick={this.onClickFavorite}
                                />,
                                <Button
                                    key="submit"
                                    type={this.checkItemCarted()}
                                    icon="shopping-cart"
                                    size="large"
                                    shape="circle"
                                    onClick={this.onClickBuyTicket}
                                />
                            ]}
                        >
                            <div style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginBottom: '16px'
                            }}>
                                <img src={item.image_url} style={{ height: '300px', width: 'auto', paddingTop: '16px' }} />
                            </div>
                            <h3>{"Price : " + item.attenuation_level + "$"}</h3>
                            <p>
                                <b>Tagline:</b> {item.tagline}
                            </p>
                            <p>
                                <b>First Brewed:</b> {item.first_brewed}
                            </p>
                            <p>
                                <b>Description:</b> {item.description}
                            </p>
                            <p>
                                <b>Brewers Tips:</b> {item.brewers_tips}
                            </p>
                            <p>
                                <b>Contributed by:</b> {item.contributed_by}
                            </p>
                        </Modal>
                    ) : (
                            <div />
                        )
                }
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);