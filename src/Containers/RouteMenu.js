import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ListBeer from '../Components/beer/listBeer';
import ListFavorite from './FavoritePage';
import ListCart from './CartPage';
import ProfilePage from '../Components/profile/ProfilePage';
import BeerPage from './BeerPage';

function RouteMenu (props) {
    return (
        <div style={{ width: '100%' }}>
        <Switch>
            {/* <Route
                path="/home"
                exact
                render={() => {
                    return (
                        <ListBeer 
                            items={props.items}
                            // onItemMovieClick={props.onItemMovieClick}
                        />
                    );
                }}
            /> */}
            <Route exact path="/home" component={BeerPage} />
            <Route path="/favorite" exact component={ListFavorite} />
            <Route path="/cart" exact component={ListCart} />
            <Route path="/profile" exact component={ProfilePage} />
            <Redirect from="*" exact to="/" />
        </Switch>
        </div>
    )
}

export default RouteMenu