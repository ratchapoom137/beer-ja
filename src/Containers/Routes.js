import React from 'react'
import { Route, Switch } from 'react-router-dom'
import LoginPage from './LoginPage'
import MainPage from './MainPage'
import RegisterPage from './RegisterPage'
// import CartPage from '../Containers/CartPage'

function Routes() {
    return (
        <div style={{ width: '100%' }}>
            <Switch> {/* ต้องใส่ switch มาคอบ route เพราะว่าไม่งั้น component จะซ้อนกัน ต้องใส่ switch เพื่อสลับไปอีกหน้า */}
                <Route exact path="/" component={LoginPage} />
                <Route exact path="/register" component={RegisterPage} />
                {/* <Route exact path="/cart" component={CartPage} /> */}
                <Route component={MainPage} />

            </Switch>
        </div>
    )
}

export default Routes