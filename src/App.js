import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './App.css';
import Routes from './Containers/Routes'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Routes />
      </div>
    );
  }
}

export default App;
