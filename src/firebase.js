import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

var config = {
    apiKey: "AIzaSyDzbeBBUWHY56esdSSzo1-2PwVm_MGJZ7E",
    authDomain: "dv-workshop.firebaseapp.com",
    databaseURL: "https://dv-workshop.firebaseio.com",
    projectId: "dv-workshop",
    storageBucket: "dv-workshop.appspot.com",
    messagingSenderId: "617987518707"
  };
  firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

  export { database, auth, provider}